const express = require('express');
const cors = require('cors');

const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post('/submit', (req, res) => {
  const { nome, email } = req.body;

  if (!nome || !email) {
    return res.status(400).send('Por favor, preencha todos os campos.');
  }

  console.log(`Nome: ${nome}, E-mail: ${email}`);
  res.status(200).send('Dados recebidos com sucesso!');
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Servidor rodando na porta ${PORT}`);
});


